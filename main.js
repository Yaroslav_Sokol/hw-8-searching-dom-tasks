// 1. Це інтерфейс який дозволяє отримати доступ до вмісту документа, також змінювати його за допомогою скриптів.
// 2. 'innerHTML' повертає або задає вміст елемента, включаючи всі теги та їх вміст, коли 'innerText' повертає тільки текст, без тегів.
// 3. 'document.getElementById()' - цей метод повертає елемент за його "id";
//    'document.querySelector()' - цей метод дозволяє знайти перший елемент, який відповідає CSS-селектору;
//    'document.getElementsByClassName()' - цей метод повертає елементи, які мають вказаний клас;
//    'document.getElementsByTagName()' - цей метод повертає елементи з вказаним тегом;
//    'document.getElementsByName()' - цей метод повертає елементи з вказаним атрибутом 'name', але на данний момент цей метод unuseless;
//    Який метод використовувать залежить від задачі та ситуації, але найблішь кращі способи 'document.getElementById()' та 'document.querySelector()'

// Встановлення кольору фона, на всі параграфи
let allParagraphs = document.getElementsByTagName("p");
for(let i = 0; i < allParagraphs.length; i++){
    allParagraphs[i].style.backgroundColor = "#ff0000";
  }

// Знаходження елемtнта з ID "optionList"
let optionsList = document.getElementById("optionsList");
console.log(optionsList);

// Знаходження батькывського елемента
let parentElement = optionsList.parentElement;
console.log(parentElement);

// Дочірні ноди, назва та тип нод
let childNodes = optionsList.childNodes;

for (let i = 0; i < childNodes.length; i++) {
    const nodeName = childNodes[i].nodeName;
    const nodeType = childNodes[i].nodeType;
    console.log("Node Name:", nodeName, "Node Type:", nodeType);
}

// Задав текст елементу з ID 'testParagraph'
let content = "This is a paragraph";
document.getElementById("testParagraph").innerHTML = content;

// Знаходимо елемент із заданим класом, після отримуємо всі вкладені елементи
let mainHeader = document.querySelector('.main-header');
let nestedElements = mainHeader.querySelectorAll('*');

// Перебираємо кожен елемент та додаєм до них клас "nav-item"
for (let i = 0; i < nestedElements.length; i++) {
  nestedElements[i].classList.add('nav-item');
  console.log(nestedElements[i]);
}

// Знаходим кожний елемент із заданим класом
let sectionTitles = document.querySelectorAll('.section-title');

// Перебіраєи кожен елемент та видаляєм клас
for (let i = 0; i < sectionTitles.length; i++) {
  sectionTitles[i].classList.remove('section-title');
}